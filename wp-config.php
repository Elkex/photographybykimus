<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p8bCYj)7kjx+TPjR!_JeY%xTZ,}[CKnrGcQR#]XXIpo?xieR8~Gf]N}C/?,w03:b' );
define( 'SECURE_AUTH_KEY',  'q/xg5[U{%<?K)^Eu5gl!^<*j-r>F&AbOG>`xy8D#/dxHO|4C>#J}zC5=wi2$4EuA' );
define( 'LOGGED_IN_KEY',    'I/si%w_d8ml$y}zOBIIZ$ <>jX{>Uif<0<QE{q*nCs,,`,fXf2iWt[OgfLi S9;X' );
define( 'NONCE_KEY',        ']^Ox^AJ:cFc!Q&m?[$vWH.6F!%RKI/DMm2Ck_?5@u(to(Xtd(#yHto@Z.dw!wks`' );
define( 'AUTH_SALT',        'VgQesdqQ2wUU99~Qg im11SHheK)$Sv-2%5-PK=Jqj J(]~oNv$Y;:!_zoVHHFXL' );
define( 'SECURE_AUTH_SALT', 'jrQ0navl5!mxh1#7.1zc`!Ncj-C YDd )p,w0Y .<=%>3_/sHW+9x`,m*KR#3PAU' );
define( 'LOGGED_IN_SALT',   '_HSLtty=Y#j6j:gcY_nfOrL<~:&Pd^3de,y1BIB0=,^)x0~XPNpvHCGlnopM *F0' );
define( 'NONCE_SALT',       'A$hw[tinp5h|CIN,mBj=)hi [rRXm<nF<bzqSm4{;BTFC2@f[{Y{LIx>NnJA/uOK' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
