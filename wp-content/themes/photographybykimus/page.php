<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav-dark'); ?>

<main class="c-main" role="main">
    <section class="g-m-y-xxl">
        <div class="g-container">
            <h1 class="e-text-center"><?php echo the_title(); ?></h1>
        </div>

            <?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    echo the_content();
                }
            }
            ?>
    </section>
</main>

<?php get_template_part('partials/footer'); ?>