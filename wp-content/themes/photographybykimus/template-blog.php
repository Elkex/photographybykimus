<?php
/*
 * Template Name: Template Blog
 */
?>
<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav-dark'); ?>
<main class="c-main" role="main">
    <section class="g-m-y-xl">
        <div class="g-container">
            <h1 class="e-text-center">
                <?php echo the_title(); ?>
            </h1>
            <div class="e-text-container">
                <?php echo the_content(); ?>
            </div>
            <div class="g-grid">
                <?php

                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $args = array(
                    'posts_per_page' => 6,
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'paged' => $paged,
                );

                $loop = new WP_Query($args);

                while ($loop->have_posts()) : $loop->the_post(); ?>
                    <article class="g-col g-col-12 g-col-4-lg g-flex g-flex-column">
                        <div class="g-p-x-lg g-p-y-sm">
                            <div class="c-blog e-text-center">
                                <div>
                                    <?php echo get_the_post_thumbnail(); ?>
                                    <h3>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                    <p class="c-blog-date">
                                        <?php the_date(); ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <?php the_excerpt(); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </article>

                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>

                <?php if ($loop->max_num_pages > 1) : ?>

                    <div class="g-col g-col-12">
                        <nav class="c-pagination g-flex g-flex-justify-content-center g-m-y-xl">
                            <div class="g-m-x-sm">
                                <?php echo get_previous_posts_link('< Vorige pagina', $loop->max_num_pages); ?>
                            </div>
                            <div class="g-m-x-sm">
                                <?php echo get_next_posts_link('Volgende pagina >', $loop->max_num_pages); ?>
                            </div>
                        </nav>
                    </div>
                <?php endif; ?>


            </div>
        </div>
    </section>
</main>
<?php get_template_part('partials/footer'); ?>