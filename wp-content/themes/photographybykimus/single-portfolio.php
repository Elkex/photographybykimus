<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav-dark'); ?>

<section class="g-m-top-xxl">
	<div class="g-container">
		<h1 class="e-text-center">
			<?php echo the_title(); ?>
		</h1>
		<?php
		if (have_posts()) {
			while (have_posts()) {
				the_post();
				echo the_content();
			}
		}
		?>
	</div>
</section>

<section class="g-m-top-xl g-m-b-xxl u-bg-light">
	<div class="g-container">
		<div class="g-grid">
			<div class="g-col g-col-12">
				<?php
				$images = get_field('portfolio_item_slider');
				if ($images) : ?>
					<!-- Image slider -->
					<div class="c-portfolio-placeholder e-text-center">
						<p class="e-text-center">
							Een ogenblik de foto's worden geladen...
						</p>
					</div>
					<div class="c-portfolio g-flex g-flex-wrap">

						<?php foreach ($images as $image) : ?>
							<a data-fancybox="gallery" href="<?php echo esc_url($image['url']); ?>" class="c-portfolio-item g-p-xs">
								<img src="<?php echo esc_url($image['url']); ?>" srcset="" alt="<?php echo esc_url($image['url']); ?>" />
							</a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<section class="c-portfolio-contact g-p-y-xxl g-m-top-xxl">
	<div class="g-container">
		<h3 class="e-text-center">
			Geïnteresseerd in een fotoshoot?
		</h3>
		<div class="g-flex g-flex-justify-content-center g-m-top-xl">
		<a href="/contact" class="c-button c-button-secondary">
			Ik wil een afspraak
		</a>
		</div>
	</div>
</section>

<?php get_template_part('partials/footer'); ?>