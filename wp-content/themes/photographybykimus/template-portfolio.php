<?php
/*
 * Template Name: Template Portfolio
 */
?>
<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav-dark'); ?>
<main class="c-main" role="main">
    <section class="g-m-y-xl">
        <div class="g-container">
            <h1 class="e-text-center">
                <?php echo the_title(); ?>
            </h1>
            <div class="e-text-container">
            <?php echo the_content(); ?>
            </div>
            <div class="g-grid">
                <?php

                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $args = array(
                    'posts_per_page' => 6,
                    'post_type' => 'portfolio',
                    'post_status' => 'publish',
                    'paged' => $paged,
                );

                $loop = new WP_Query($args);

                while ($loop->have_posts()) : $loop->the_post(); ?>
                    <article class="g-col g-col-12 g-col-6-lg">
                        <div class="g-p-x-lg g-p-y-sm">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="c-card c-card-portfolio">
                                <?php echo get_the_post_thumbnail(); ?>
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                            </a>
                        </div>
                    </article>

                <?php endwhile; ?>

                <?php wp_reset_postdata(); ?>

                <?php if ($loop->max_num_pages > 1) : ?>

                    <div class="g-col g-col-12">
                        <nav class="c-pagination g-flex g-flex-justify-content-center g-m-y-xl">
                            <div class="g-m-x-sm">
                                <?php echo get_previous_posts_link('< Vorige pagina', $loop->max_num_pages); ?>
                            </div>
                            <div class="g-m-x-sm">
                                <?php echo get_next_posts_link('Volgende pagina >', $loop->max_num_pages); ?>
                            </div>
                        </nav>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </section>
</main>
<?php get_template_part('partials/footer'); ?>