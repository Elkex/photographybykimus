<?php
/*
 * Template Name: Template Portfolio
 */
?>
<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav-dark'); ?>
<main class="c-main" role="main">
    <section class="g-m-y-xl">
        <div class="g-container">
            <h1>
                <?php echo the_title(); ?>
            </h1>
            <div class="g-grid">
                <?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                $args = array(
                    'posts_per_page' => 6,
                    'post_type' => 'portfolio',
                    'post_status' => 'publish',
                    'paged' => $paged,
                );

                $args = new WP_Query($args);

                while ($args->have_posts()) : $args->the_post(); ?>
                    <article class="g-col g-col-12 g-col-6-lg">
                    <div class="g-p-x-lg g-p-y-sm">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="c-card">
                            <?php echo get_the_post_thumbnail(); ?>
                            <h2>
                                <?php the_title(); ?>
                            </h2>
                        </a>
                    </div>
                    </article>

                <?php endwhile; ?>
                <?php if ($paged > 1) : ?>
                    <div class="g-col g-col-12">
                        <nav class="g-flex g-flex-justify-content-between g-m-y-xl">
                            <?php previous_posts_link('&laquo; Nieuwere posts '); ?>

                            <?php next_posts_link('Oudere posts &raquo;'); ?>
                        </nav>
                    </div>
                <?php else : ?>
                    <div class="g-col g-col-12">
                        <nav class="g-flex g-flex-justify-content-end g-m-y-xl">
                            <?php next_posts_link('Oudere posts &raquo;'); ?>
                        </nav>
                    </div>

                <?php endif; ?>

                <?php wp_reset_postdata(); ?>

            </div>
        </div>
    </section>
</main>
<?php get_template_part('partials/footer'); ?>