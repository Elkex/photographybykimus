<footer class="c-footer g-p-y-lg g-m-top-xxl" role="contentinfo">
    <div class="g-container g-p-y-md">
        <div class="g-grid">
            <div class="g-col g-col-12 g-col-3-md g-flex g-flex-column g-flex-align-items-center g-flex-align-items-start-md">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo_photographybykimus.svg" alt="Logo" class="c-nav-logo" width="130" height="auto">
                </a>
            </div>
            <div class="g-col g-col-12 g-col-3-md g-flex g-flex-column g-flex-align-items-center g-flex-align-items-start-md">
                <h4 class="g-m-top-lg g-m-top-none-lg">Contact ons</h4>
                <?php the_field('options_adress', 'option'); ?>
                <div class="g-m-y-xs">
                    <a href="tel:<?php the_field('options_phone', 'option'); ?>">
                        <?php the_field('options_phone', 'option'); ?>
                    </a>
                </div>
            </div>
            <div class="g-col g-col-12 g-col-3-md g-flex g-flex-column g-flex-align-items-center g-flex-align-items-start-md">
                <h4 class="g-m-top-lg g-m-top-none-lg">Menu</h4>
                <nav class="c-footer-nav">
                    <ul>
                        <?php foreach (get_menu_items('primary') as $item) : ?>
                            <li>
                                <a href="<?php echo $item['url']; ?>" class="p-nav-item">
                                    <?php echo $item['name']; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
            </div>
            <div class="g-col g-col-12 g-col-3-md g-flex g-flex-column g-flex-align-items-center g-flex-align-items-start-md">
                <h4 class="g-m-top-lg g-m-top-none-lg">Volg ons via social media</h4>
                <nav class="c-footer-nav">
                    <ul class="g-flex">
                        <?php if (get_field('options_facebook', 'options')) : ?>
                            <li class="g-m-right-xl">
                                <a href="<?php echo the_field('options_facebook', 'options'); ?>">
                                    <svg class='c-icon c-icon-facebook g-m-right-xs'>
                                        <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#facebook'></use>
                                    </svg>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (get_field('options_instagram', 'options')) : ?>
                            <li class="g-m-right-xl">
                                <a href="<?php echo the_field('options_instagram', 'options'); ?>">
                                    <svg class='c-icon c-icon-instagram g-m-right-xs'>
                                        <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#instagram'></use>
                                    </svg>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (get_field('options_linkedin', 'options')) : ?>
                            <li class="g-m-right-xl">
                                <a href="<?php echo the_field('options_linkedin', 'options'); ?>">
                                    <svg class='c-icon c-icon-linkedin g-m-right-xs'>
                                        <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#linkedin'></use>
                                    </svg>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (get_field('options_twitter', 'options')) : ?>
                            <li class="g-m-right-xl">
                                <a href="<?php echo the_field('options_twitter', 'options'); ?>">
                                    <svg class='c-icon c-icon-twitter g-m-right-xs'>
                                        <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#twitter'></use>
                                    </svg>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="c-footer-sub">
        <hr />
        <div class="g-grid">
            <div class="g-container">
                <div class="g-col g-col-12">
                    <div class="g-flex g-flex-column g-flex-align-items-center g-flex-row-md g-flex-justify-content-between-md">
                        <p>
                            <small>
                                &copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?> | <a href="/privacy-policy">Privacy Policy</a>
                            </small>
                        </p>
                        <p>
                            <small>
                                Webdesign & Development by <a href="https://www.kleurcode.be" title="HTML5 Blank">Kleurcode</a>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>

</body>

</html>