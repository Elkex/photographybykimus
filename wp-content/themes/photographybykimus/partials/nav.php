<div class="p-nav">
    <div class="p-nav-top g-p-y-xs">
        <div class="g-container">
            <div class="g-flex g-flex-column g-flex-row-xl g-flex-align-items-center g-flex-justify-content-between">
                <div class="g-flex g-flex-column g-flex-align-items-center g-flex-row-xl">
                    <a href="tel:<?php echo the_field('options_phone', 'options'); ?>" target="_blank" title="LinkedIn" class="g-flex g-flex-align-content-center g-m-right-md-lg g-m-bottom-md g-m-bottom-none-lg">
                        <svg class='c-icon c-icon-phone g-m-right-xs'>
                            <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#phone'></use>
                        </svg>
                        <?php echo the_field('options_phone', 'options'); ?>
                    </a>
                    <a href="mailto:<?php echo the_field('options_email', 'options'); ?>" target="_blank" title="LinkedIn" class="g-flex g-flex-align-content-center">
                        <svg class='c-icon c-icon-mail g-m-right-xs'>
                            <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#mail'></use>
                        </svg>
                        <?php echo the_field('options_email', 'options'); ?>
                    </a>
                    <span class="g-none g-block-xl g-m-x-sm">|</span>
                    <div class="g-flex g-m-top-md g-m-top-none-xl">
                        <?php if (get_field('options_facebook', 'options')) : ?>
                            <a href="<?php echo the_field('options_facebook', 'options'); ?>">
                                <svg class='c-icon c-icon-facebook g-m-right-xs'>
                                    <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#facebook'></use>
                                </svg>
                            </a>
                        <?php endif; ?>
                        <?php if (get_field('options_instagram', 'options')) : ?>
                            <a href="<?php echo the_field('options_instagram', 'options'); ?>">
                                <svg class='c-icon c-icon-instagram g-m-right-xs'>
                                    <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#instagram'></use>
                                </svg>
                            </a>
                        <?php endif; ?>
                        <?php if (get_field('options_linkedin', 'options')) : ?>
                            <a href="<?php echo the_field('options_linkedin', 'options'); ?>">
                                <svg class='c-icon c-icon-linkedin g-m-right-xs'>
                                    <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#linkedin'></use>
                                </svg>
                            </a>
                        <?php endif; ?>
                        <?php if (get_field('options_twitter', 'options')) : ?>
                            <a href="<?php echo the_field('options_twitter', 'options'); ?>">
                                <svg class='c-icon c-icon-twitter g-m-right-xs'>
                                    <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#twitter'></use>
                                </svg>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="g-flex g-flex-column g-flex-row-xl">
                    <nav class="g-flex g-flex-wrap g-p-y-md g-p-y-none-xl">
                        <a href="/over-mij" title="Over ons" class="g-m-right-md">
                            Over mij
                        </a>
                        <a href="/faq" title="FAQ" class="g-m-right-md">
                            FAQ
                        </a>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <nav>
        <div class="g-container">
            <div class="g-flex g-flex-column g-flex-row-xl g-flex-justify-content-between">
                <div class="g-flex g-flex-justify-content-between g-flex-align-items-center g-p-y-sm">
                    <a href="/" title="Home">
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo_photographybykimus_white.svg" alt="PhotographybyKimus logo" title="PhotographybyKimus logo" class="p-nav-logo" />
                    </a>
                    <a href="#" class="p-nav-toggle g-block g-none-xl">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <nav class="p-nav-main g-m-top-xl g-m-top-none-xl g-none g-flex-xl g-flex-column g-flex-row-xl g-flex-align-items-center">
                    <ul class="g-m-none g-flex g-flex-align-items-center g-flex-column g-flex-row-xl">
                        <?php
                        global $post;
                        $postId = $post->ID;
                        //var_dump(wp_get_nav_menu_items('primary'));
                        foreach (get_menu_items('primary') as $item) : ?>
                            <?php if ($item['children']) : ?>
                                <li class="p-subnav-wrapper">
                                    <div class="g-flex g-flex-justify-content-between">
                                        <?php global $post; ?>
                                        <a href="<?php echo $item['url']; ?>" title="Kleding" class="p-nav-button-title g-p-y-lg-xl g-p-y-md <?php echo $item['id'] == get_queried_object_id() ? 'p-nav-button-active' : ''; ?>">
                                            <?php echo $item['name']; ?>
                                            <svg class='c-icon c-icon-chevron-down p-nav-button-icon'>
                                                <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#chevron-down'></use>
                                            </svg>
                                        </a>
                                        <button href="#" class="g-p-md g-block g-none-xl js-toggle">
                                            <svg class='c-icon c-icon-chevron-down'>
                                                <use xlink:href='<?php echo get_template_directory_uri(); ?>/dist/sprite/sprite.svg#chevron-down'></use>
                                            </svg>
                                        </button>
                                    </div>
                                    <div class="p-nav-subnav g-p-md-xl">
                                        <div class="g-flex g-flex-row">
                                            <ul>
                                                <?php foreach ($item['children'] as $child) : ?>
                                                    <li>
                                                        <a href="<?php echo $child['url']; ?>">
                                                            <?php echo $child['name']; ?>
                                                        </a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            <?php else : ?>
                                <li>
                                    <a href="<?php echo $item['url']; ?>" title="<?php echo $item['name']; ?>" class="p-nav-button-title g-p-y-lg-xl g-p-y-md <?php echo $item['id'] == $postId ? 'p-nav-button-active' : ''; ?>">
                                        <?php echo $item['name']; ?>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </nav>
            </div>
        </div>
    </nav>
</div>