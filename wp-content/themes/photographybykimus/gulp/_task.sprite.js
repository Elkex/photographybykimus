const gulp = require('gulp'),
    del = require('del'),
    util = require('gulp-util'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    sprite = require('gulp-svg-sprite');

gulp.task('sprite', () => {
    gulp.src(settings.sprite.src + '**/*.svg')
        .pipe(plumber(function (error) {
            util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
            this.emit('end');
        }))
        .pipe(sprite({
            mode: {
                symbol: {
                    dest: './',
                    sprite: settings.sprite.filename,
                }
            },
            shape: {
                id: {
                    generator: '%s'
                }
            },
            svg: {
                xmlDeclaration: false,
                doctypeDeclaration: false
            }
        }))
        .pipe(gulp.dest(settings.sprite.dist))
        .pipe(browserSync.stream());
});