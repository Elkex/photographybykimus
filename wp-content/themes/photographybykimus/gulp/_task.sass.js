var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    sass = require('gulp-sass'),
    gulpif = require('gulp-if'),
    cssnano = require('gulp-cssnano'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    cmq = require('gulp-combine-mq'),
    plumber = require('gulp-plumber'),
    util = require('gulp-util'),
    cleanCSS = require('gulp-clean-css');

gulp.task('sass', function () {
    gulp.src(settings.sass.src + '**/*.scss')
        .pipe(plumber(function (error) {
            util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({style: 'compressed'}))
        .pipe(autoprefixer(settings.sass.autoprefixer))
        .pipe(cmq())
        .pipe(gulpif(settings.production, cssnano()))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(settings.sass.dist))
        .pipe(browserSync.reload({
            stream: true
        }));
});