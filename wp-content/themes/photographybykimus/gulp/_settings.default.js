global.settings = {
  production: false,
  browserSync: {
    url: 'http://photographybykimus.test/'
  },
  templates: {
    src: 'src/templates/',
    dist: 'dist/',
    build: false
  },
  sprite: {
    src: 'src/sprite/',
    dist: 'dist/sprite/',
    filename: 'sprite.svg'
  },
  sass: {
    src: 'src/css/',
    dist: '',
    autoprefixer: {
      browsers: [
        "> 1%",
        "last 3 versions",
        "IE 11",
        "IE 10"
      ]
    }
  },
  vendor: {
    src: 'src/vendor/',
    dist: 'dist/vendor/'
  },
  js: {
    src: 'src/js/',
    components: 'src/js/components/',
    dist: 'dist/js/',
    filename: 'main.js'
  },
  images: {
    src: 'src/img/',
    dist: 'dist/img/'
  },
  fonts: {
    src: 'src/fonts/',
    dist: 'dist/fonts/'
  }
};