var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    util = require('gulp-util'),
    del = require('del');

gulp.task('fonts', function() {
  del.sync(settings.fonts.dist);
  gulp.src(settings.fonts.src + '*.{ttf,otf,eot,svg,woff,woff2}')
    .pipe(plumber(function(error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))
  .pipe(gulp.dest(settings.fonts.dist))
  .pipe(browserSync.reload({
    stream: true
  }));
});