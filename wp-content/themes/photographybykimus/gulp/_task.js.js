var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    gulpif = require('gulp-if'),
    stripDebug = require('gulp-strip-debug'),
    sourcemaps = require('gulp-sourcemaps'),
    plumber = require('gulp-plumber'),
    util = require('gulp-util');

gulp.task('js', function() {
  gulp.src([settings.js.components + '*.js', settings.js.src + '*.js'])
    .pipe(plumber(function(error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))
    .pipe(sourcemaps.init())
    .pipe(concat(settings.js.filename))
    .pipe(gulpif(settings.production, uglify()))
    .pipe(gulpif(settings.production, stripDebug()))
    .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest(settings.js.dist))
  .pipe(browserSync.reload({
    stream: true
  }));
});