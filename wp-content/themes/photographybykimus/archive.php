<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav'); ?>

<section>
	<div class="g-container">
		<h1>
			<?php echo the_title(); ?>
		</h1>
		<?php
		if (have_posts()) {
			while (have_posts()) {
				the_post();
				echo the_content();
			}
		}
		?>
	</div>
</section>

<?php get_template_part('partials/footer'); ?>