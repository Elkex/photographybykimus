<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav'); ?>

<section>
    <div class="g-container">
        <h1>
            <?php echo the_title(); ?>
        </h1>
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : ?>
                <?php the_post(); ?>
                <?php echo the_title();
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>

<?php get_template_part('partials/footer'); ?>