<?php
/*
 *  Author: Elke Moras | @elkemoras
 *  URL: kleurcode.be
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// Load HTML5 Blank scripts (header.php)
function kleurcode_scripts()
{
    wp_enqueue_script('kleurcode_theme_js', get_template_directory_uri() . '/dist/js/main.js', array('jquery'), '1.0.0');
    wp_enqueue_script('kleurcode_imagesloaded', get_template_directory_uri() . '/dist/vendor/imagesloaded.pkgd.min.js', array(), '1.0.0');
    wp_enqueue_script('kleurcode_masonry', get_template_directory_uri() . '/dist/vendor/masonry.pkgd.min.js', array(), '1.0.0');
    wp_enqueue_script('kleurcode_fancybox', get_template_directory_uri() . '/dist/vendor/jquery.fancybox.min.js', array(), '1.0.0');
}

add_action('wp_enqueue_scripts', 'kleurcode_scripts');

// LOAD STYLES
function kleurcode_styles()
{
    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
}

add_action('wp_enqueue_scripts', 'kleurcode_styles'); // Add Theme Stylesheet

// Add logo to customizer

add_theme_support('custom-logo', array(
    'height' => 100,
    'width' => 400,
    'flex-height' => true,
    'flex-width' => true,
    'header-text' => array('site-title', 'site-description'),
));

// Add ACF Options
if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

// Creëer custom post types
function custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Portfolio', 'Post Type General Name', 'photographybykimus' ),
            'singular_name'       => _x( 'Portfolio item', 'Post Type Singular Name', 'photographybykimus' ),
            'menu_name'           => __( 'Portfolio', 'photographybykimus' ),
            'parent_item_colon'   => __( 'Parent Portfolio item', 'photographybykimus' ),
            'all_items'           => __( 'All Portfolio', 'photographybykimus' ),
            'view_item'           => __( 'View Portfolio item', 'photographybykimus' ),
            'add_new_item'        => __( 'Add New Portfolio item', 'photographybykimus' ),
            'add_new'             => __( 'Add New', 'photographybykimus' ),
            'edit_item'           => __( 'Edit Portfolio item', 'photographybykimus' ),
            'update_item'         => __( 'Update Portfolio item', 'photographybykimus' ),
            'search_items'        => __( 'Search Portfolio item', 'photographybykimus' ),
            'not_found'           => __( 'Not Found', 'photographybykimus' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'photographybykimus' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'Portfolio', 'photographybykimus' ),
            'description'         => __( 'Portfolio items', 'photographybykimus' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
            'hierarchical'        => false,
            'menu_icon' => 'dashicons-camera',
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
             
            // This is where we add taxonomies to our CPT
            'taxonomies'          => array( 'category' ),
        );
         
        // Registering your Custom Post Type
        register_post_type( 'Portfolio', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
    add_action( 'init', 'custom_post_type', 0 );

/*
** Create Mega Menu
*/

function get_menu_items($nav)
{
    foreach (wp_get_nav_menu_items($nav) as $item) {
        if ($item->menu_item_parent == "0") {
            $mainItemList[] = array('id' => $item->object_id, 'name' => $item->title, 'url' => $item->url);
        };
    };

    foreach ($mainItemList as $subItem) :
        foreach (wp_get_nav_menu_items($nav) as $item) :
            if ($subItem['id'] == $item->menu_item_parent) :
                $subItem['children'][] = array('id' => $item->object_id, 'name' => $item->title, 'url' => $item->url, 'parentid' => $item->menu_item_parent);
            else :
                $subItem = $subItem;
            endif;
        endforeach;
        $menu[] = $subItem;
    endforeach;
    return $menu;
};
