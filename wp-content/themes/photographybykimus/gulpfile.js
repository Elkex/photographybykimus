'use strict';

var gulp = require('gulp');

require('./gulp/_settings.default.js');
require('./gulp/_task.browsersync.js');
require('./gulp/_task.templates.js');
require('./gulp/_task.sprite.js');
require('./gulp/_task.sass.js');
require('./gulp/_task.vendor.js');
require('./gulp/_task.js.js');
require('./gulp/_task.images.js');
require('./gulp/_task.fonts.js');
require('./gulp/_task.watch.js');

gulp.task('default', ['browsersync', 'templates', 'sprite', 'sass', 'vendor', 'js', 'images', 'fonts', 'watch']);