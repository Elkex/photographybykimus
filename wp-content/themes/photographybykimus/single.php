<?php get_template_part('partials/head'); ?>
<?php get_template_part('partials/nav-dark'); ?>

<section class="g-m-y-xxl">
	<div class="g-container">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php get_the_post_thumbnail(); ?>

				<div class="g-m-y-xl">
					<div class="g-grid">
						<div class="g-col g-col-12 g-col-8-lg g-offset-2-lg">
							<h1>
								<?php echo the_title(); ?>
							</h1>
							<?php echo the_content(); ?>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
				</div>
</section>

<?php get_template_part('partials/footer'); ?>