jQuery(document).ready(function ($) {
    navigation.init();
    
    var $grid = $('.c-portfolio').imagesLoaded(

        function () {
            $('.c-portfolio-placeholder').hide();
            $('.c-portfolio').fadeIn();
            // init Masonry after all images have loaded
            $grid.masonry({
                itemSelector: '.c-portfolio-item',
                percentPosition: true,
                columnWidth: '.c-portfolio-item',
            });

        }
    );

});