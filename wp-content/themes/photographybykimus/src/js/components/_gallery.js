gallery = {
    init: function () {
        if (jQuery('.c-slider-slick').length > 0) {
      
            // fancybox
            jQuery('[data-fancybox="gallery"]').fancybox({
              afterShow: function (instance, slide) {
                
                // Tip: Each event passes useful information within the event object:
                
                // Object containing references to interface elements
                // (background, buttons, caption, etc)
                // console.info( instance.jQueryrefs );
                
                // Current slide options
                // console.info( slide.opts );
                
                // Clicked element
                //console.info( slide.opts.jQueryorig );
                
                // Reference to DOM element of the slide
                //console.info( slide.jQueryslide );
                
                let idx = jQuery.fancybox.getInstance().currIndex;
                
                jQuery('.c-slider-slick-for').slick('slickGoTo', idx);
                jQuery('.c-slider-slick-nav').slick('slickGoTo', idx);
                
              },
              thumbs: {
                showOnStart: true
              },
              hash: false
            })
            
            jQuery('.c-slider-slick-for').on('init', function (event, slick) {
              
              if (jQuery('html').hasClass('no-touch')) {
                jQuery('.zoom').ezPlus({
                  responsive: true,
                  easing: true
                });
              }
            })
            
            jQuery('.c-slider-slick-for').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: false,
              asNavFor: '.c-slider-slick-nav',
              adaptiveHeight: false,
              variableWidth: false,
              centerMode: false,
              dots: false,
              swipeToSlide: true,
              lazyLoad: 'progressive',
              infinite: false,
              nextArrow: '.next-arrow',
              prevArrow: '.prev-arrow',
              responsive: [
                {
                  breakpoint: 960,
                  settings: {
                    arrows: false,
                  }
                },
                {
                  breakpoint: 640,
                  settings: {
                    arrows: true
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    arrows: true
                  }
                }
              ]
            });
            
            jQuery('.c-slider-slick-nav').slick({
              slidesToShow: 7,
              slidesToScroll: 1,
              asNavFor: '.c-slider-slick-for',
              dots: false,
              centerMode: false,
              focusOnSelect: true,
              arrows: false,
              adaptiveHeight: false,
              variableWidth: false,
              infinite: false,
              nextArrow: '.next-arrow',
              prevArrow: '.prev-arrow',
            });
            
            jQuery('.next-arrow').on('click', function () {
              if (jQuery('.c-slider-slick-nav .slick-slide').length <= 5) {
            
                var currSlide = jQuery('.c-slider-slick-nav').slick('slickCurrentSlide');
                jQuery('.c-slider-slick-nav').slick('slickGoTo', currSlide + 1, 'false');
                jQuery('.c-slider-slick-nav .slick-track').css('transform', 'translate3d(0px, 0px, 0px)');
              } else {
                jQuery('.c-slider-slick-nav').slick('slickNext');
              }
            });
            jQuery('.prev-arrow').on('click', function () {
              if (jQuery('.c-slider-slick-nav .slick-slide').length <= 5) {
                var currSlide = jQuery('.c-slider-slick-nav').slick('slickCurrentSlide');
                jQuery('.c-slider-slick-nav').slick('slickGoTo', currSlide - 1, 'false');
                jQuery('.c-slider-slick-nav .slick-track').css('transform', 'translate3d(0px, 0px, 0px)');
              } else {
                jQuery('.c-slider-slick-nav').slick('slickPrev');
              }
            });
            
          }
    }
}