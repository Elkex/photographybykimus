slider = {
    init: function () {
        jQuery('.c-slider-slick-home').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            adaptiveHeight: false,
            variableWidth: false,
            centerMode: false,
            dots: true,
            swipeToSlide: true,
            lazyLoad: 'progressive',
            infinite: false,
            nextArrow: '.next-arrow',
            prevArrow: '.prev-arrow',
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        arrows: false,
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        arrows: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: true
                    }
                }
            ]
        });

    }
}