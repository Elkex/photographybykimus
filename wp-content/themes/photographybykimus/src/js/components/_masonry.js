masonry = {
    init : function() {
        console.log("init masonry");
        $('.js-masonry').masonry({
            itemSelector: '.l-portfolio-thumbnail',
            columnWidth: 200
        });
    }
}