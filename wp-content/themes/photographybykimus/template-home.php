<?php
/*
 *  Template Name: Homepage
 */
?>
<?php get_template_part('partials/head'); ?>


<section class="p-header p-nav-space-top">
    <figure class="">
        <div class="u-img-bg">
            <?php echo get_the_post_thumbnail(); ?>
        </div>
        <?php include('partials/nav.php'); ?>
        <figcaption class="g-flex g-flex-column g-flex-row-xl g-flex-justify-content-center g-flex-align-items-center">
            <div class="g-container">
                <div class="g-grid">
                    <div class="g-col g-col-12 g-col-8-xl">
                        <div class="p-header-content">
                            <h1>
                                <?php echo the_title(); ?>
                            </h1>
                            <div class="g-flex g-flex-align-items-center p-m-top-lg">
                                <a href="/contact" class="c-button c-button-primary g-m-right-md">Ik heb een vraag</a>
                                <a href="/mijn-portfolio" class="c-button c-button-primary-ghost">Portfolio</a>
                            </div>
                        </div>
                    </div>
                </div>
        </figcaption>
    </figure>
</section>

<main class="c-main" role="main">
    <?php
    if (have_posts()) : ?>
        <?php while (have_posts()) : ?>
            <?php the_post(); ?>
            <?php echo the_content(); ?>
        <?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_template_part('partials/footer'); ?>